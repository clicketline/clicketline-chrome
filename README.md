<img src="clicketline/icons/megaphone.png" height=200px style="float:right" alt="clicketline!">

# clicketline-chrome - don't cross the picketline, online

In the before times when workers went on strike, you'd see them out the front of their place of work, you'd see their signs.

You'd know they are fighting and you'd either join the line or do your shopping another day.

Now days the shop is on line, and robots can take orders any time of the day or night. Even if the staff are on strike the machine
runs, the payments get processed and the money gets made. 

This extension is designed to alert you if the staff behind a website are on strike, so you can show solidarity.

After all, nobody wants to be a scab.

# Reporting strikes.

You can submit a pull request. 

# Get in touch
<a rel="me" href="https://union.place/@clicketline">https://union.place/@clicketline</a>
