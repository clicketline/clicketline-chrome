/* MAGIC */
var domain_to_message = {
  'com' : { "nytimes": 1}
  
};
var strikes = [
	[[null,null,"This is a test strike, no workers were harmed in the making of this dialogue box", 'http://cta-page.com', 'article.com']],
	[[null,null,"The staff at the NYTimes are on strike!", "", ""]],
];
var annoying_logging=true;
/* END OF MAGIC */

// in test mode of the plugin blocks couchdb's web interface on localhost
if (annoying_logging) {
	domain_to_message['localhost:5984']=0 
}

let title= "Nobody wants to be a scab! Don't cross the picket line!";

function add_banner(list){
	
	let [start_sec,end_sec,message,cta,article] = list;

let html_warning =(`
	<div style="position:absolute; top:10; height:20%; left:5%; width:90%; background-color:#F33; color:#000; z-index:1000!important">
		<h1 style="display:block">HEADING</h1>
		<p>MESSAGE
	` +
	(article? ` <p>You can <a href="READMORE">read more about the strike here</a>. ` :"" ) +
	(cta?     ` <p>You can <a href="HELP">help contribute to the strike here</a>.  ` :"" ) +
	`
	</div>
	`).replace("MESSAGE",  message).replace("HEADING", title).replace("READMORE",article).replace("HELP",cta);
	document.body.appendChild( new DOMParser().parseFromString(html_warning,"text/html").body.firstChild );

}

function check_url(url) {
	let match = url.host.split('.').reverse();
	let now = Date.now();
	console.log("clicketline: ", "url=", url, "match=",match, "now=",now);

	let m= domain_to_message;
	for (let i =0; i < match.length; i++) {
		let label = m[ match[i] ];
		if ('number' === typeof label) {
			annoying_logging && console.log("domain match! " , label);

			let ranges = strikes[ label ];
			for(let j=0; j < ranges.length; j++) {
				let r=ranges[j];
				if ( null===r[0] || r[0] < now ) {
					annoying_logging && console.log("Strike started", r);
					if ( null===r[1] || r[1] > now ) {
						console.log("On strike! Do not cross the picket line! ");
						add_banner(r);

						return;
					}
					else {
						annoying_logging && console.log("Strike ended: ", r);
					}
				}
			}
			console.log( "No current strikes, hope they got what they asked for!");
			return
		}
		else if('object' === typeof label) {
			annoying_logging && console.log("more domain: ", match[i], label);
			m = label
		}
		else {
			annoying_logging && console.log("??", label, typeof label);
			return
		}
		annoying_logging && console.log( "No strikes for ", url.host);
	}
}
check_url(document.location);
